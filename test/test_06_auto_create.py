from flask import Flask
import pytest

from flask_statics import *

### Fixtures

@pytest.fixture
def app():
    return Flask(__name__)

@pytest.fixture
def undefined_var():
    return "oof"

@pytest.fixture
def initial_value():
    return "value"

@pytest.fixture
def later_value():
    return "later"

@pytest.fixture
def defined_var(app, initial_value):
    name = "foo"
    update(app, name, initial_value)
    return name

@pytest.fixture
def unrelated_var():
    return "plover"

### Tests

def test_fixture_appropriate_vars(undefined_var, defined_var, unrelated_var):
    # We also use these strings in literal attribute exprs below.
    assert undefined_var == "oof"
    assert defined_var == "foo"
    assert unrelated_var == "plover"

def test_fixture_appropriate_values(initial_value, later_value):
    assert initial_value is not None
    assert later_value is not None
    assert initial_value != later_value

def test_register_auto_create_after_define_raises(app, defined_var):
    create = lambda: "oops"

    with pytest.raises(HandlerClashError):
        register_auto_var(app, defined_var, create=create)

def test_register_auto_create_different_raises(app, undefined_var):
    create1 = lambda: "oops"
    create2 = lambda: "oooops"

    register_auto_var(app, undefined_var, create=create1)
    with pytest.raises(HandlerClashError):
        register_auto_var(app, undefined_var, create=create2)

def test_register_auto_create_identical_succeeds(app, undefined_var):
    create = lambda: "oops"

    register_auto_var(app, undefined_var, create=create)
    register_auto_var(app, undefined_var, create=create)

def test_register_auto_create_identical_after_define_succeeds(app, undefined_var):
    create = lambda: "oops"

    register_auto_var(app, undefined_var, create=create)
    get_or_create(app, undefined_var)
    register_auto_var(app, undefined_var, create=create)

def test_auto_create_once(app, undefined_var):
    calls = 0
    auto_value = "auto"
    def create():
        nonlocal calls
        calls += 1
        return auto_value

    register_auto_var(app, undefined_var, create=create)
    result = get_or_create(app, undefined_var)
    assert result is auto_value
    assert calls == 1

def test_auto_create_only_once(app, undefined_var):
    calls = 0
    auto_value = "auto"
    def create():
        nonlocal calls
        calls += 1
        return auto_value

    register_auto_var(app, undefined_var, create=create)
    get_or_create(app, undefined_var)
    result2 = get_or_create(app, undefined_var)
    assert result2 is auto_value
    assert calls == 1

def test_auto_create_after_delete(app, undefined_var):
    calls = 0
    prefix = "auto"
    def create():
        nonlocal calls
        calls += 1
        return prefix + str(calls)

    register_auto_var(app, undefined_var, create=create)
    result1 = get_or_create(app, undefined_var)
    assert calls == 1
    assert result1 == "auto1"

    delete(app, undefined_var)

    result2 = get_or_create(app, undefined_var)
    assert calls == 2
    assert result2 == "auto2"
