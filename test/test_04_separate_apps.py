from flask import Flask
import pytest

from flask_statics import *

### Fixtures

@pytest.fixture
def app1():
    return Flask(__name__)

@pytest.fixture
def app2():
    return Flask(__name__)

@pytest.fixture
def value1():
    return "one"

@pytest.fixture
def value2():
    return "two"

@pytest.fixture
def value3():
    return "three"

@pytest.fixture
def shared_var():
    return "plover"

@pytest.fixture
def var_in_1(app1, shared_var, value1):
    update(app1, shared_var, value1)
    return shared_var

@pytest.fixture
def var_in_2(app2, shared_var, value2):
    update(app2, shared_var, value2)
    return shared_var

### Tests

def test_fixture_same_var(shared_var, var_in_1, var_in_2):
    assert shared_var == var_in_1
    assert shared_var == var_in_2

def test_fixture_distinct_values(value1, value2, value3):
    assert value1 != value2
    assert value2 != value3
    assert value1 != value3

def test_separate_app_get(app1, app2, shared_var, var_in_1, var_in_2, value1, value2):
    assert get_existing(app1, shared_var) is value1
    assert get_existing(app2, shared_var) is value2

def test_only_first_app(app1, app2, shared_var, var_in_1, value1):
    assert get_existing(app1, shared_var) is value1
    assert not is_defined(app2, shared_var)

def test_only_second_app(app1, app2, shared_var, var_in_2, value2):
    assert not is_defined(app1, shared_var)
    assert get_existing(app2, shared_var) is value2

def test_separate_app_update_1(app1, app2, shared_var, var_in_1, var_in_2, value1, value2, value3):
    update(app1, shared_var, value3)
    assert get_existing(app1, shared_var) is value3
    assert get_existing(app2, shared_var) is value2

def test_separate_app_update_2(app1, app2, shared_var, var_in_1, var_in_2, value1, value2, value3):
    update(app2, shared_var, value3)
    assert get_existing(app1, shared_var) is value1
    assert get_existing(app2, shared_var) is value3

def test_separate_app_delete_1(app1, app2, shared_var, var_in_1, var_in_2, value1, value2, value3):
    delete(app1, shared_var)
    assert not is_defined(app1, shared_var)
    assert get_existing(app2, shared_var) is value2

def test_separate_app_delete_2(app1, app2, shared_var, var_in_1, var_in_2, value1, value2, value3):
    delete(app2, shared_var)
    assert get_existing(app1, shared_var) is value1
    assert not is_defined(app2, shared_var)

def test_separate_app_delete_all_1(
    app1, app2, shared_var, var_in_1, var_in_2, value1, value2, value3
):
    delete_all(app1)
    assert not is_defined(app1, shared_var)
    assert get_existing(app2, shared_var) is value2

def test_separate_app_delete_all_2(
    app1, app2, shared_var, var_in_1, var_in_2, value1, value2, value3
):
    delete_all(app2)
    assert get_existing(app1, shared_var) is value1
    assert not is_defined(app2, shared_var)
