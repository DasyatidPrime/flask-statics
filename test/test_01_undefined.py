from flask import Flask
import pytest

from flask_statics import *

### Fixtures

@pytest.fixture
def app():
    return Flask(__name__)

@pytest.fixture(params=['never_define', 'define_then_undefine'])
def undefined_var(request, app):
    name = "plugh"
    if request.param == 'never_define':
        pass
    elif request.param == 'define_then_undefine':
        update(app, name, True)
        delete(app, name)
    else:
        raise RuntimeError("weird undefined_var param")
    return name

@pytest.fixture
def if_absent():
    return "absent"

### Tests

def test_is_defined_false(app, undefined_var):
    assert not is_defined(app, undefined_var)

def test_get_passive_none(app, undefined_var):
    assert get_passive(app, undefined_var) is None

def test_get_passive_default(app, undefined_var, if_absent):
    assert get_passive(app, undefined_var, if_absent) is if_absent

def test_get_existing_raises(app, undefined_var):
    with pytest.raises(AppStaticNameError):
        get_existing(app, undefined_var)

def test_get_with_refresh_none(app, undefined_var):
    assert get_with_refresh(app, undefined_var) is None

def test_get_with_refresh_default(app, undefined_var, if_absent):
    assert get_with_refresh(app, undefined_var, if_absent) is if_absent

def test_get_or_create_raises(app, undefined_var):
    with pytest.raises(NoHandlerError):
        get_or_create(app, undefined_var)

def test_delete_raises(app, undefined_var):
    with pytest.raises(AppStaticNameError):
        delete(app, undefined_var)

def test_delete_all_succeeds(app, undefined_var):
    delete_all(app)
    assert True
