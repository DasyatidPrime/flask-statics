import pytest

from flask_statics import *

### No fixtures

### Tests

def test_app_static_name_error():
    assert isinstance(AppStaticNameError, type)
    assert NameError in AppStaticNameError.mro()

def test_no_handler_error():
    assert isinstance(NoHandlerError, type)
    assert Exception in NoHandlerError.mro()

def test_handler_clash_error():
    assert isinstance(HandlerClashError, type)
    assert Exception in HandlerClashError.mro()

def test_handler_value_error():
    assert isinstance(HandlerValueError, type)
    assert ValueError in HandlerValueError.mro()
