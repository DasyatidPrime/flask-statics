from flask import Flask
import pytest

from flask_statics import *

### Fixtures

@pytest.fixture
def app():
    return Flask(__name__)

@pytest.fixture
def app_context(app):
    with app.app_context():
        yield app

@pytest.fixture
def undefined_var():
    return "oof"

@pytest.fixture
def initial_value():
    return "value"

@pytest.fixture
def later_value():
    return "later"

@pytest.fixture
def defined_var(app, initial_value):
    name = "foo"
    update(app, name, initial_value)
    return name

@pytest.fixture
def unrelated_var():
    return "plover"

### Tests

def test_fixture_appropriate_vars(undefined_var, defined_var, unrelated_var):
    # We also use these strings in literal attribute exprs below.
    assert undefined_var == "oof"
    assert defined_var == "foo"
    assert unrelated_var == "plover"

def test_fixture_appropriate_values(initial_value, later_value):
    assert initial_value is not None
    assert later_value is not None
    assert initial_value != later_value

def test_getitem_no_context_raises(unrelated_var):
    with pytest.raises(Exception):
        gk[unrelated_var]

def test_setitem_no_context_raises(unrelated_var):
    with pytest.raises(Exception):
        gk[unrelated_var] = unrelated_var

def test_delitem_no_context_raises(unrelated_var):
    with pytest.raises(Exception):
        del gk[unrelated_var]

def test_getattr_no_context_raises(unrelated_var):
    with pytest.raises(Exception):
        gk.plover

def test_setattr_no_context_raises(unrelated_var):
    with pytest.raises(Exception):
        gk.plover = "plover"

def test_delattr_no_context_raises(unrelated_var):
    with pytest.raises(Exception):
        del gk.plover

def test_getitem_undefined_raises(app_context, undefined_var):
    with pytest.raises(KeyError):
        gk[undefined_var]

def test_delitem_undefined_raises(app_context, undefined_var):
    with pytest.raises(KeyError):
        del gk[undefined_var]

def test_getattr_undefined_raises(app_context, undefined_var):
    with pytest.raises(AttributeError):
        gk.oof

def test_delattr_undefined_raises(app_context, undefined_var):
    with pytest.raises(AttributeError):
        del gk.oof

def test_getitem_defined_returns(app_context, defined_var, initial_value):
    assert gk[defined_var] is initial_value

def test_delitem_defined_succeeds(app, app_context, defined_var):
    del gk[defined_var]
    assert not is_defined(app, defined_var)

def test_getattr_defined_returns(app_context, defined_var, initial_value):
    assert gk.foo is initial_value

def test_delattr_defined_succeeds(app, app_context, defined_var):
    del gk.foo
    assert not is_defined(app, defined_var)

def test_setitem_getfunc_round_trip(app, app_context, undefined_var, later_value):
    gk[undefined_var] = later_value
    assert get_existing(app, undefined_var) is later_value

def test_setattr_getfunc_round_trip(app, app_context, undefined_var, later_value):
    gk.oof = later_value
    assert get_existing(app, undefined_var) is later_value

def test_setfunc_getitem_round_trip(app, app_context, undefined_var, later_value):
    update(app, undefined_var, later_value)
    assert gk[undefined_var] is later_value

def test_setfunc_getattr_round_trip(app, app_context, undefined_var, later_value):
    update(app, undefined_var, later_value)
    assert gk.oof is later_value

def test_setitem_getattr_round_trip(app, app_context, undefined_var, later_value):
    gk[undefined_var] = later_value
    assert gk.oof is later_value

def test_setattr_getitem_round_trip(app, app_context, undefined_var, later_value):
    gk.oof = later_value
    assert gk[undefined_var] is later_value
