from flask import Flask
import pytest

from flask_statics import *

### Fixtures

@pytest.fixture
def app():
    return Flask(__name__)

@pytest.fixture
def initial_value():
    return "value"

@pytest.fixture
def defined_var(app, initial_value):
    name = "plugh"
    update(app, name, initial_value)
    return name

@pytest.fixture
def if_absent():
    return "absent"

### Tests

def test_is_defined_true(app, defined_var):
    assert is_defined(app, defined_var)

def test_get_passive_round_trip(app, defined_var, initial_value):
    assert get_passive(app, defined_var) is initial_value

def test_get_existing_round_trip(app, defined_var, initial_value):
    assert get_existing(app, defined_var) is initial_value

def test_get_with_refresh_raises(app, defined_var, initial_value):
    with pytest.raises(NoHandlerError):
        get_with_refresh(app, defined_var)

def test_get_or_create_round_trip(app, defined_var, initial_value):
    assert get_or_create(app, defined_var) is initial_value

def test_update_changes_value(app, defined_var, initial_value):
    new_value = (initial_value,)
    update(app, defined_var, new_value)
    assert get_existing(app, defined_var) is new_value

def test_delete_makes_not_defined(app, defined_var):
    delete(app, defined_var)
    assert not is_defined(app, defined_var)

def test_delete_all_makes_not_defined(app, defined_var):
    delete_all(app)
    assert not is_defined(app, defined_var)
