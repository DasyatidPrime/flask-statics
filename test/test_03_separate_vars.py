from flask import Flask
import pytest

from flask_statics import *

### Fixtures

@pytest.fixture
def app():
    return Flask(__name__)

@pytest.fixture
def value1():
    return "one"

@pytest.fixture
def value2():
    return "two"

@pytest.fixture
def value3():
    return "three"

@pytest.fixture
def var1(app, value1):
    name = "plugh"
    update(app, name, value1)
    return name

@pytest.fixture
def var2(app, value2):
    name = "xyzzy"
    update(app, name, value2)
    return name

### Tests

def test_fixture_distinct_vars(var1, var2):
    assert var1 != var2

def test_fixture_distinct_values(value1, value2, value3):
    assert value1 != value2
    assert value2 != value3
    assert value1 != value3

def test_separate_get(app, var1, var2, value1, value2):
    assert get_existing(app, var1) is value1
    assert get_existing(app, var2) is value2

def test_separate_update_1(app, var1, var2, value1, value2, value3):
    update(app, var1, value3)
    assert get_existing(app, var1) is value3
    assert get_existing(app, var2) is value2

def test_separate_update_2(app, var1, var2, value1, value2, value3):
    update(app, var2, value3)
    assert get_existing(app, var1) is value1
    assert get_existing(app, var2) is value3

def test_separate_delete_1(app, var1, var2, value1, value2):
    delete(app, var1)
    assert not is_defined(app, var1)
    assert get_existing(app, var2) is value2

def test_separate_delete_2(app, var1, var2, value1, value2):
    delete(app, var2)
    assert get_existing(app, var1) is value1
    assert not is_defined(app, var2)

def test_delete_all_deletes_both(app, var1, var2):
    delete_all(app)
    assert not is_defined(app, var1)
    assert not is_defined(app, var2)
    assert get_passive(app, var1) is None
    assert get_passive(app, var2) is None
