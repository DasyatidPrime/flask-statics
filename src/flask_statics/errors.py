__all__ = [
    "AppStaticNameError",
    "NoHandlerError",
    "HandlerClashError",
    "HandlerValueError",
]


class AppStaticNameError(NameError):
    def __init__(self, var_name):
        super().__init__(self, var_name)
        self.var_name = var_name

    def __str__(self):
        return "app-static variable '{}' is not defined".format(self.var_name)


class NoHandlerError(LookupError):
    def __init__(self, handler_slot, var_name):
        super().__init__(self, handler_slot, var_name)
        self.handler_slot = handler_slot
        self.var_name = var_name

    def __str__(self):
        return "no {} handler available for app-static variable '{}'".format(
            self.handler_slot, self.var_name
        )


class HandlerClashError(Exception):
    def __init__(self, var_name):
        super().__init__(self, var_name)
        self.var_name = var_name

    def __str__(self):
        return "not overwriting handlers for app-static variable '{}' (delete them first)".format(
            self.var_name
        )


class HandlerValueError(ValueError):
    def __init__(self, handler_slot, var_name, value, problem):
        super().__init__(self, handler_slot, var_name, value, problem)
        self.handler_slot = handler_slot
        self.var_name = var_name
        self.value = value
        self.problem = problem

    def __str__(self):
        return "{} handler for app-static variable '{}' returned {}, which incorrectly {}".format(
            self.handler_slot, self.var_name, repr(self.value), self.problem
        )
