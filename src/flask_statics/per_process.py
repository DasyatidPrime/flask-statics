__all__ = [
    "per_process_storage",
]

import threading
from weakref import WeakKeyDictionary

from .errors import *
from .per_app import PerAppStorage


class PerProcessStorage(object):
    __slots__ = ["dict", "lock"]

    def __init__(self):
        self.dict = WeakKeyDictionary()
        self.lock = threading.Lock()

    def __getitem__(self, app):
        with self.lock:
            found = self.dict.get(app, None)
            if found is None:
                found = PerAppStorage()
                self.dict[app] = found
            return found

    def get(self, app, if_absent=None):
        with self.lock:
            return self.dict.get(app, if_absent)

    def __delitem__(self, app):
        with self.lock:
            del self.dict[app]


per_process_storage = PerProcessStorage()
