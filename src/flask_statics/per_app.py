__all__ = [
    "PerAppStorage",
]

import threading

from .errors import *
from .variable import VariableStorage


class PerAppStorage(object):
    __slots__ = ["vars", "lock"]

    def __init__(self):
        self.vars = dict()
        self.lock = threading.Lock()

    def _get_var(self, var_name):
        with self.lock:
            return self.vars.get(var_name)

    def _get_or_create_var(self, var_name):
        with self.lock:
            try:
                return self.vars[var_name]
            except KeyError:
                new_var = VariableStorage(var_name)
                self.vars[var_name] = new_var
                return new_var

    def apply_handlers(self, var_name, **handlers):
        self._get_or_create_var(var_name).apply_handlers(**handlers)

    def get_passive(self, var_name, if_absent=None):
        var = self._get_var(var_name)
        if var is None:
            return if_absent
        return var.get_value_or_return(if_absent)

    def get_existing(self, var_name):
        var = self._get_var(var_name)
        if var is None:
            raise AppStaticNameError(var_name)
        return var.get_value_or_raise()

    def get_with_refresh(self, var_name, if_absent=None, refresh=None):
        var = self._get_var(var_name)
        if var is None:
            return if_absent
        return var.get_and_refresh_value_or_return(if_absent, refresh)

    def get_or_create(self, var_name, create=None, refresh=None):
        return self._get_or_create_var(var_name).get_or_create_value(create, refresh)

    def set_var(self, var_name, value):
        self._get_or_create_var(var_name).set_value(value)

    def delete_var(self, var_name, also_handlers=False):
        with self.lock:
            try:
                var = self.vars[var_name]
                if also_handlers or not var.has_any_handlers():
                    del self.vars[var_name]
            except KeyError:
                raise AppStaticNameError(var_name) from None

        # We still have to do this even if we unlinked the variable storage above, because it would
        # be an error to return and then have another thread return a value for the variable
        # without an intervening set.
        var.delete_value()

    def has_var(self, var_name):
        var = self._get_var(var_name)
        if var is None:
            return False
        return var.has_value()
