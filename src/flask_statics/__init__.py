"""Keeping data in memory between requests.

Flask normally pops the application context between requests, but
often there's static-ish data that's useful to keep in memory for a
longer period, where you're willing to require an app restart if it
changes, or where checking for freshness is easy.

App-static variables are thread-safe to access, and are stored once
per app per process.[1]_ Note that for an app running in a multi-process
configuration, this is *not* the same as once per app! This is not a
substitute for a database or a filesystem; it is a way to avoid doing
things like re-parsing large static configuration files before every
request. If you need an in-memory cache that *can* be shared between
processes, automatically persisted on disk, etc., then this is not
the module you need.[2]_

To allow lazily creating and refreshing an app-static variable when
it is accessed, you can use `register_auto_var`. For instance, when
loading a configuration file, you could provide a create handler
that loads it unconditionally, and a refresh handler that stats the
file and reloads it if it is different. You can of course do all of
that yourself if you prefer.

Using this module has benefits over certain other approaches:

- Basic accesses and provided lazy-update patterns are thread-safe,
  so you don't have to worry about manually handling locks around
  global state or else making your app fail unpredictably in a
  multi-threading environment.

- It doesn't pollute the app object itself with attributes in a way
  that either requires manual name mangling or risks collisions.

- It doesn't clash with Flask app configuration values, which have
  their own name and value conventions which are unsuited to the
  types of data objects this module is intended for.

.. (TODO) I mean, *is* that what people do... ?

- It nonetheless binds the variables to the app object rather than
  to the entire Python interprete, so that running more than one
  (possibly differently configured) instance of the same Flask app
  in the same process (such as via WSGI dispatch middleware) isn't
  precluded.

Providing a Werkzeug proxy (such as `flask.current_app`) as an app
to any public function in this module acts as though you provided the
underlying app object. In this case, it is an error if there is no
underlying app object. By passing a specific app object, you may access
app-static variables for it through the function interface regardless
of the current Flask context, but `gpp` always uses `flask.current_app`,
so it is an error to use `gpp` outside a Flask application context.

When an app object is garbage collected, its app-static variables
may later be collected if not otherwise reachable; they do not leak.
For more determinism, you can use `delete_all` to explicitly delete
all the variables for an app at once. There is currently no means for
registering static teardown handlers; you should ensure that all
app-static variables are safe to discard at the end of each request,
or when the app might otherwise go away.

.. [1] Technically, per Python interpreter.

.. [2] It could, however, be a reasonable building block for such a
   module.
"""

__all__ = [
    "AppStaticNameError",
    "NoHandlerError",
    "HandlerClashError",
    "HandlerValueError",
    "register_auto_var",
    "is_defined",
    "get_passive",
    "get_existing",
    "get_with_refresh",
    "get_or_create",
    "update",
    "delete",
    "delete_all",
    "gk",
]

from .errors import *
from .functions import *
from .proxy import gk

__version__ = "0.0.1"
