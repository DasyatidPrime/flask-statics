__all__ = [
    "VariableStorage",
]

import threading

from .errors import *


class VariableStorage(object):
    __slots__ = ["value", "lock", "name", "create", "refresh"]
    _handler_slots = ["create", "refresh"]

    def __init__(self, name, *, create=None, refresh=None):
        # The value attribute acts like the variable proper: it is initially undefined, and may
        # transition between defined and undefined during use.
        self.lock = threading.Lock()
        self.name = name
        self.create = create
        self.refresh = refresh

    def has_any_handlers(self):
        return any(getattr(self, slot) is not None for slot in self._handler_slots)

    def apply_handlers(self, **handlers):
        with self.lock:
            for key in handlers.keys():
                if key not in self._handler_slots:
                    raise KeyError("{} is not an app-static variable handler slot".format(key))

            if self.has_any_handlers():
                raise HandlerClashError(self.name)

            for slot, handler in handlers.items():
                setattr(self, slot, handler)

    def get_value_or_raise(self):
        with self.lock:
            try:
                return self.value
            except AttributeError:
                raise AppStaticNameError(self.name) from None

    def get_value_or_return(self, if_absent):
        with self.lock:
            try:
                return self.value
            except AttributeError:
                return if_absent

    # Lock is held on entry.
    def _handle_refresh(self, stored_value, override_handler):
        if override_handler is None:
            refresh = self.refresh
        else:
            refresh = override_handler
        if refresh is None:
            raise NoHandlerError("refresh", self.name)

        new_value = refresh(stored_value)
        if new_value is None:
            raise HandlerValueError("refresh", self.name, new_value, "is None")

        if new_value is not stored_value:
            self.value = new_value
            return new_value
        else:
            return stored_value

    def get_and_refresh_value_or_return(self, if_absent, override_refresh):
        with self.lock:
            try:
                found = self.value
            except AttributeError:
                return if_absent
            return self._handle_refresh(found, override_refresh)

    # Lock is held on entry.
    def _handle_create(self, override_handler):
        if override_handler is not None:
            create = override_handler
        else:
            create = self.create
        if create is None:
            raise NoHandlerError("create", self.name)

        new_value = create()
        if new_value is None:
            raise NoHandlerError("create", self.name, new_value, "is None")

        self.value = new_value
        return new_value

    def get_or_create_value(self, override_create, override_refresh):
        with self.lock:
            try:
                found = self.value
            except AttributeError:
                try:
                    return self._handle_create(override_create)
                except NoHandlerError as exc:
                    raise exc from None
            else:
                try:
                    return self._handle_refresh(found, override_refresh)
                except NoHandlerError:
                    return found

    def set_value(self, value):
        with self.lock:
            if value is None:
                if self.create is not None:
                    format_string = (
                        "cannot set auto-create variable '{}' to None" + " (delete it instead)"
                    )
                    raise TypeError(format_string.format(self.name))
                if self.refresh is not None:
                    format_string = (
                        "cannot set auto-refresh variable '{}' to None" + " (delete it instead)"
                    )
                    raise TypeError(format_string.format(self.name))
            self.value = value

    def delete_value(self):
        with self.lock:
            try:
                del self.value
            except AttributeError:
                raise AppStaticNameError(self.name) from None

    def has_value(self):
        with self.lock:
            return hasattr(self, "value")
