__all__ = [
    "register_auto_var",
    "is_defined",
    "get_passive",
    "get_existing",
    "get_with_refresh",
    "get_or_create",
    "update",
    "delete",
    "delete_all",
]

from werkzeug.local import LocalProxy

from .errors import *
from .per_process import per_process_storage


def _unproxy(obj):
    if isinstance(obj, LocalProxy):
        return obj._get_current_object()
    else:
        return obj


def register_auto_var(app, var, *, create=None, refresh=None):
    """Install lazy-update-on-fetch handlers for an app-static variable.

    If `create` is provided, it will be called when `get_or_create`
    finds the variable unset. The value returned by the function becomes
    the variable's initial value. To prevent subtle misuses, returning
    `None` from `create` is an error; use a dummy object or raise an
    exception instead.

    If `refresh` is provided, it will be called when `get_with_refresh`
    or `get_or_create` finds the variable set, receiving the current value
    of the variable as its only argument. `refresh` normally checks whether
    the value is "fresh" (in whatever way is appropriate to the application)
    and returns either the original value or an updated value. In any case,
    the value returned from `refresh` becomes the new value of the variable
    and is then returned from `get_with_refresh` or `get_or_create`.

    To prevent subtle misuses, when an app-static variable has a create or
    refresh handler, it may never be explicitly set to `None`; however, it
    may be deleted to achieve a similar effect. If you need to check
    whether an app-static variable is present without calling any of
    its handlers, you can use `get_passively`.

    Note that while a handler is running, any accesses to the variable from
    other threads will be blocked until it is finished. A long-running
    handler may cause performance issues in a multi-threading environment."""
    per_process_storage[_unproxy(app)].apply_handlers(var, create=create, refresh=refresh)


def is_defined(app, var):
    """Return true if an app-static variable has a value."""
    this_app_storage = per_process_storage.get(_unproxy(app))
    if this_app_storage is None:
        return False
    return this_app_storage.has_var(var)


def get_passive(app, var, if_absent=None):
    """Fetch an app-static variable, without lazy update.

    If the variable has no value, `if_absent` (which defaults to `None`)
    is returned. Otherwise, the value of the variable is returned.
    No create or refresh handlers for the variable are called, but it is
    not an error if such handlers exist."""
    this_app_storage = per_process_storage.get(_unproxy(app))
    if this_app_storage is None:
        return if_absent
    return this_app_storage.get_passive(var, if_absent)


def get_existing(app, var):
    """Fetch an already-defined app-static variable, without lazy update.

    If the variable has no value, an `AppStaticNameError` (which is also
    a `NameError`) is raised. Otherwise, the value of the variable is
    returned. No create or refresh handlers for the variable are called,
    but it is not an error if such handlers exist."""
    this_app_storage = per_process_storage.get(_unproxy(app))
    if this_app_storage is None:
        raise AppStaticNameError(var)
    return this_app_storage.get_existing(var)


def get_with_refresh(app, var, if_absent=None, refresh=None):
    """Fetch a lazy-update app-static variable, refreshing it if set.

    If the variable is unset, `if_absent` (which defaults to `None`)
    is returned. Otherwise, the value of the variable is passed through
    its refresh handler, normally installed via `register_auto_var`,
    and either the original value or the refreshed value is returned.

    If `refresh` is not `None`, it becomes the refresh handler for this call
    only, including if no refresh handler was installed. If the variable is
    set, but no refresh handler was previously installed nor provided as
    `refresh`, then a `NoHandlerError` is raised.

    See `register_auto_var` for the semantics of variable handlers."""
    this_app_storage = per_process_storage.get(_unproxy(app))
    if this_app_storage is None:
        return if_absent
    return this_app_storage.get_with_refresh(var, if_absent, refresh)


def get_or_create(app, var, create=None, refresh=None):
    """Fetch a lazy-update app-static variable, creating it if unset.

    If the variable is unset, its create handler, normally installed
    via `register_auto_var`, is called to produce the initial value,
    and this value is returned. Otherwise, the value of the variable
    is passed through its refresh handler, and either the original
    value or the refreshed value is returned.

    If `create` is not `None`, it becomes the create handler for this call
    only, including if no create handler was installed. If the variable was
    unset, and no create handler was previously installed nor provided as
    `create`, then a `NoHandlerError` is raised.

    The refresh behavior and the semantics of the `refresh` argument
    are identical to those of `get_with_refresh`, except that it is not
    an error if the variable is set and there is no refresh handler;
    in that case, the value is assumed to be fresh. Newly created values
    are also assumed to be fresh.

    See `register_auto_var` for the semantics of variable handlers."""
    return per_process_storage[_unproxy(app)].get_or_create(var, create, refresh)


def update(app, var, new_value):
    """Set an app-static variable to a new value.

    Note that per `register_auto_var`, a variable with lazy-update handlers
    may not be explicitly set to `None`. In most cases, you should delete
    the variable instead."""
    per_process_storage[_unproxy(app)].set_var(var, new_value)


def delete(app, var, also_handlers=False):
    """Delete an app-static variable, leaving it with no value.

    Any handlers from `register_auto_var` remain attached to the variable
    name, unless `also_handlers` is `True`, in which case all handlers are
    uninstalled. If the variable did not already have a value, an
    `AppStaticNameError` is raised."""
    this_app_storage = per_process_storage.get(_unproxy(app))
    if this_app_storage is None:
        raise AppStaticNameError(var)
    this_app_storage.delete_var(var, also_handlers)


def delete_all(app):
    """Delete all app-static variables for an app.

    This also deletes all handlers for all variable names for that app.
    It is not an error if the app had no variables."""
    try:
        del per_process_storage[_unproxy(app)]
    except KeyError:
        pass
