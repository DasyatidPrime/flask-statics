__all__ = [
    "gpp",
]

from flask import current_app

from .errors import *
from .functions import get_or_create, update, delete


class CurrentAppStaticsProxy(object):
    __slots__ = []

    def __init__(self):
        pass

    # Note that we can't just set the get/set/del methods to the same ones, because the error
    # behavior differs subtly: the item methods raise a KeyError and the attribute ones an
    # AttributeError in case of an expected variable not existing. We could factor that out,
    # but it'd be a lot of indirection...

    def __getitem__(self, var):
        try:
            return get_or_create(current_app, var)
        except NoHandlerError as exc:
            if exc.handler_slot == "create":
                raise KeyError(var) from None
            else:
                raise

    def __setitem__(self, var, value):
        return update(current_app, var, value)

    def __delitem__(self, var):
        try:
            return delete(current_app, var)
        except AppStaticNameError as exc:
            if exc.var_name == var:
                raise KeyError(var) from None
            else:
                raise

    def __getattr__(self, var):
        try:
            return get_or_create(current_app, var)
        except NoHandlerError as exc:
            if exc.handler_slot == "create":
                raise AttributeError(var) from None
            else:
                raise

    def __setattr__(self, var, value):
        return update(current_app, var, value)

    def __delattr__(self, var):
        try:
            return delete(current_app, var)
        except AppStaticNameError as exc:
            if exc.var_name == var:
                raise AttributeError(var) from None
            else:
                raise


gk = CurrentAppStaticsProxy()
"""Convenience proxy for the current app's app-static variables.

Variables can be accessed by getting, setting, or deleting items or
attributes (interchangeably) on this object:

- Getting an item or attribute acts like `get_or_create`. This means
  variables with registered lazy-update handlers will be created or
  refreshed on access as appropriate.

- Setting an item or attribute acts like `update`.

- Deleting an item or attribute acts like `delete` with `also_handlers`
  set to its default value of `False`. This means any lazy-update
  handlers remain installed even when the value is removed.

The error behavior also differs slightly from the function interface:
if an `AppStaticNameError` would have been raised due to an undefined
variable, or a `NoHandlerError` would have been raised for a missing
create handler, a `KeyError` or `AttributeError` (depending on the mode
of access) is raised instead.

Other semantics, such as those of `get_passive`, `get_with_refresh`,
or `delete` with `also_handlers=True`, are not available through this
proxy; you must use the function interface for those.
"""
